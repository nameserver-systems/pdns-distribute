package servicediscovery

import "errors"

var (
	errNoURL = errors.New("ServiceDiscovery: No URL was set")
)
